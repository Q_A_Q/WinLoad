
// TestWinLoadDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "TestWinLoad.h"
#include "TestWinLoadDlg.h"
#include "afxdialogex.h"
#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CTestWinLoadDlg 对话框



CTestWinLoadDlg::CTestWinLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_TESTWINLOAD_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestWinLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTestWinLoadDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON1, &CTestWinLoadDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CTestWinLoadDlg 消息处理程序

BOOL CTestWinLoadDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	std::string strPipName = "XXX";
	CString strObjtectName = TEXT("\\\\.\\Pipe\\");
	strObjtectName += strPipName.c_str();
	//BOOL bRet = WaitNamedPipe(strObjtectName, NMPWAIT_WAIT_FOREVER);
	//if (false == bRet)
	//{
	//	//AfxMessageBox(L"连接管道失败");
	//	return;
	//}
	m_hPipe = CreateFile(			//管道属于一种特殊的文件
		strObjtectName,	//创建的文件名
		GENERIC_READ | GENERIC_WRITE,	//文件模式
		0,								//是否共享
		NULL,							//指向一个SECURITY_ATTRIBUTES结构的指针
		OPEN_EXISTING,					//创建参数
		FILE_ATTRIBUTE_NORMAL,			//文件属性(隐藏,只读)NORMAL为默认属性
		NULL);

	if (INVALID_HANDLE_VALUE == m_hPipe)
	{
		DWORD dw = GetLastError();
	}

	SetTimer(1, 1000, NULL);

	//启动内存dump

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CTestWinLoadDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CTestWinLoadDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CTestWinLoadDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CAboutDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值


	CDialogEx::OnTimer(nIDEvent);
}


void CTestWinLoadDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (1 == nIDEvent)
	{
		if (NULL == m_hPipe || INVALID_HANDLE_VALUE == m_hPipe)
		{
			// TODO: 在此添加额外的初始化代码
			std::string strPipName = "XXX";
			CString strObjtectName = TEXT("\\\\.\\Pipe\\");
			strObjtectName += strPipName.c_str();
			//BOOL bRet = WaitNamedPipe(strObjtectName, NMPWAIT_WAIT_FOREVER);
			//if (false == bRet)
			//{
			//	//AfxMessageBox(L"连接管道失败");
			//	return;
			//}
			m_hPipe = CreateFile(			//管道属于一种特殊的文件
				strObjtectName,	//创建的文件名
				GENERIC_READ | GENERIC_WRITE,	//文件模式
				0,								//是否共享
				NULL,							//指向一个SECURITY_ATTRIBUTES结构的指针
				OPEN_EXISTING,					//创建参数
				FILE_ATTRIBUTE_NORMAL,			//文件属性(隐藏,只读)NORMAL为默认属性
				NULL);

			if (INVALID_HANDLE_VALUE == m_hPipe)
			{
				DWORD dw = GetLastError();
				return;
		
			}

		}
		char buf[] = { 'h', 'e', 'a', 'r', 't', '_', 'b', 'e', 'a', 't', '\r', '\n'};
		DWORD nWrite = 0;
		if (!WriteFile(m_hPipe, buf, sizeof(buf), &nWrite, NULL))			//接受客户端发送数据
		{
			DWORD dw = GetLastError();
			CloseHandle(m_hPipe);
			m_hPipe = nullptr;
			AfxMessageBox(L"写管道失败");
			return;
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CTestWinLoadDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	char *p = nullptr;
	*p = 100;
}
