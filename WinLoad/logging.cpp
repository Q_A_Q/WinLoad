#include "stdafx.h"
#include "logging.h"

#include <boost/log/support/date_time.hpp>
#include <boost/log/common.hpp>
#include <boost/move/utility.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/expressions/attr.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/core.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/thread.hpp>


namespace attrs = boost::log::attributes;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;


namespace  duan
{
	BOOST_LOG_ATTRIBUTE_KEYWORD(_scope, "Scope", attrs::named_scope::value_type);

	template< typename CharT, typename TraitsT >
	inline std::basic_ostream< CharT, TraitsT >& operator<< (
		std::basic_ostream< CharT, TraitsT >& strm, severity_level lvl)
	{
		static const char* const str[] =
		{
			"TRACE",
			"DEBUG",
			"INFO ",
			"WARNN",
			"ERROR",
			"CRITI"
		};
		if (static_cast< std::size_t >(lvl) < (sizeof(str) / sizeof(*str)))
			strm << str[lvl];
		else
			strm << static_cast< int >(lvl);
		return strm;
	}

	boost::log::sources::severity_logger<severity_level> logging::m_slg;

	logging::logging(std::string strFileName)
	{
	}


	logging::~logging()
	{
	}


	bool logging::InitBoostLog(std::string strFileName)
	{


		auto sink = boost::log::add_file_log(
			//日志文件名
			boost::log::keywords::file_name = strFileName + "_%Y-%m-%d.log",

			//设置日志文件，日志输出方式
			boost::log::keywords::format = (
				boost::log::expressions::stream
				<< boost::log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S.%f [")
				//<< boost::log::expressions::attr<unsigned int>("LineID")<< "] [" 
				<< boost::log::expressions::attr<severity_level>("Severity") << "] ["
				<< boost::log::expressions::attr<attrs::current_thread_id::value_type>("ThreadID") << "] ["
				<< expr::format_named_scope(_scope, keywords::format = "%C (%F:%l)", keywords::depth = 1) << "] "
				<< boost::log::expressions::smessage
				),

			/*日志文件处理方式*/

			//单个日志大小10MB
			boost::log::keywords::rotation_size = 10 * 1024 * 1024,

			//每天重建日志文件
			boost::log::keywords::time_based_rotation = boost::log::sinks::file::rotation_at_time_point(0, 0, 0)

			//每次打开都继续写入
			//boost::log::keywords::open_mode = boost::log::
		);

		//日志文件处理
		sink->locked_backend()->set_file_collector(boost::log::sinks::file::make_collector(
			boost::log::keywords::target = "logs",
			boost::log::keywords::max_size = 30 * 1024 * 1024,
			boost::log::keywords::min_free_space = 100 * 1024 * 1024
		));


		// 如果不写这个 它不会实时的把日志写下去, 而是等待缓冲区满了,或者程序正常退出时写下,这样做的好处是减少IO操作.提高效率,  不过我这里不需要它. 因为我的程序可能会异常退出.
		sink->locked_backend()->auto_flush(true);

		//为系统日志，添加默认的time, thread id, process id等
		boost::log::add_common_attributes();
		boost::log::core::get()->add_global_attribute("ThreadID", boost::log::attributes::current_thread_id());
		boost::log::core::get()->add_global_attribute("Scope", attrs::named_scope());
		boost::log::core::get()->add_sink(sink);

		return true;
	}


	bool logging::CloseBoostLog()
	{
		boost::log::core::get()->flush();   
		boost::log::core::get()->remove_all_sinks();
		//m_sink->flush();
		//m_sink.reset();
		return true;
	}

};//end namespace