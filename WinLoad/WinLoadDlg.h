
// WinLoadDlg.h : 头文件
//

#pragma once

#include <string>
#include <boost\thread.hpp>
// CWinLoadDlg 对话框
class CWinLoadDlg : public CDialogEx
{
// 构造
public:
	CWinLoadDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_WINLOAD_DIALOG };
//#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

	bool LoadProp();

	void BeginWatch();

	bool ScanTaskManager(std::string strTaskName, DWORD &dwProcessID, bool bKillThread = false);

	bool SystemSetting();

	void ThreadPipRead(void *pThis);

	bool CreatePipe();

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

private:
	enum TimerEvent
	{
		TE_ScanTaskManager=10011,
		TE_PipReadInterval
	};

	int m_nPipReadMaxInterval;
	boost::thread *m_threadPipRead;
	CEvent m_eventExit;
	HANDLE m_hHandle;
	boost::mutex m_mutexCreatePipe;
public:
	afx_msg void OnBnClickedButtonDebugPip();
	afx_msg void OnClose();
};
