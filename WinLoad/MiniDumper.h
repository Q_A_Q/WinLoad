#ifndef MINIDUMPER_H
#define MINIDUMPER_H

#include <windows.h>
#include <dbghelp.h>


class CMiniDumper
{
public:

    CMiniDumper(bool bPromptUserForMiniDump);
    ~CMiniDumper(void);

private:

    static LONG WINAPI unhandledExceptionHandler(struct _EXCEPTION_POINTERS *pExceptionInfo);
    void setMiniDumpFileName(void);
    bool getImpersonationToken(HANDLE* phToken);
	bool getImpersonationToken(HANDLE* phToken, HANDLE hProcess);
    BOOL enablePrivilege(LPCTSTR pszPriv, HANDLE hToken, TOKEN_PRIVILEGES* ptpOld);
    BOOL restorePrivilege(HANDLE hToken, TOKEN_PRIVILEGES* ptpOld);



    _EXCEPTION_POINTERS *m_pExceptionInfo;
    TCHAR m_szMiniDumpPath[MAX_PATH];
    TCHAR m_szAppPath[MAX_PATH];
    TCHAR m_szAppBaseName[MAX_PATH];
	TCHAR m_szPipeName[MAX_PATH];
    bool m_bPromptUserForMiniDump;

	/**********************************************************************/

	bool m_IsRemoteDump;
	CEvent m_eventExit;

	/**********************************************************************/

public:
	LONG writeRemoteDump(_MINIDUMP_EXCEPTION_INFORMATION *pExceptionInfo, HANDLE hHandle, DWORD dwProcessID);
	LONG writeMiniDump(_MINIDUMP_EXCEPTION_INFORMATION *pExceptionInfo, HANDLE hHandle, DWORD dwProcessID, BOOL bIsRemoteDump = FALSE);
	BOOL OpenRemoteDump(const TCHAR* strPipeName);
    static CMiniDumper* s_pMiniDumper;
    static LPCRITICAL_SECTION s_pCriticalSection;
};

#endif // MINIDUMPER_H
