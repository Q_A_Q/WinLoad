
// WinLoad.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "WinLoad.h"
#include "WinLoadDlg.h"
#include "WinLoadIni.h"
#include "logging.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define LOAD_INI_PATH ("\\load.ini")

// CWinLoadApp

BEGIN_MESSAGE_MAP(CWinLoadApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CWinLoadApp 构造

CWinLoadApp::CWinLoadApp()
	: m_dumper(false)
{
	// 支持重新启动管理器
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CWinLoadApp 对象

CWinLoadApp theApp;


// CWinLoadApp 初始化

bool CWinLoadApp::LoadProp()
{
	HMODULE hModule = GetModuleHandle(NULL);
	char modulePath[MAX_PATH] = { 0 };
	GetModuleFileNameA(hModule, modulePath, MAX_PATH);
	CStringA strPath(modulePath);
	int nPos = strPath.ReverseFind('\\');
	if (nPos <= 0)
	{
		return false;
	}
	CStringA strIniPath = strPath.Mid(0, nPos) + LOAD_INI_PATH;

	CSingleton::GetInstance()->load(strIniPath.GetBuffer());
	strIniPath.ReleaseBuffer();
	return true;
}


BOOL CWinLoadApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。  否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();


	AfxEnableControlContainer();

	// 创建 shell 管理器，以防对话框包含
	// 任何 shell 树视图控件或 shell 列表视图控件。
	CShellManager *pShellManager = new CShellManager;

	// 激活“Windows Native”视觉管理器，以便在 MFC 控件中启用主题
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

	/************************************开始运用程序*****************************************/
	duan::logging::InitBoostLog("win_load");
	SLOG_TRACE << "开始软件监控";
	SLOG_DEBUG << "开始软件监控";
	SLOG_INFO << "开始软件监控";
	SLOG_WARNING << "开始软件监控";
	SLOG_ERROR << "开始软件监控";
	SLOG_CRITICAL << "开始软件监控";

	LoadProp();

	CWinLoadDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.Create(CWinLoadDlg::IDD);
	bool bIsSilent = (*CSingleton::GetInstance())["system"]["silent"].as<bool>();
	if (bIsSilent)
	{
		dlg.ShowWindow(SW_HIDE);
	}
	else
	{
		dlg.ShowWindow(SW_SHOW);
	}
	
	dlg.RunModalLoop();

	SLOG_INFO << "关闭软件监控";
	duan::logging::CloseBoostLog();
	// 删除上面创建的 shell 管理器。
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

#ifndef _AFXDLL
	ControlBarCleanUp();
#endif

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

