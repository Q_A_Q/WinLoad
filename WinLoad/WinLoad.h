
// WinLoad.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "MiniDumper.h"

// CWinLoadApp: 
// 有关此类的实现，请参阅 WinLoad.cpp
//

class CWinLoadApp : public CWinApp
{
public:
	CWinLoadApp();

// 重写
public:
	virtual BOOL InitInstance();

	//加载配置文件
	bool LoadProp();
	
	CMiniDumper m_dumper;


// 实现

	DECLARE_MESSAGE_MAP()
};

extern CWinLoadApp theApp;