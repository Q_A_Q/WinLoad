#include "stdafx.h"
#include "WinLoadIni.h"

ini::IniFile* CSingleton::m_pInstance = 0;

ini::IniFile* CSingleton::GetInstance()
{
	boost::mutex lock;
	lock.lock();
	if (m_pInstance == NULL)  //判断是否第一次调用
		m_pInstance = new ini::IniFile();
	lock.unlock();
	return m_pInstance;

}

