#include "StdAfx.h"

#include <windows.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <tchar.h>
#include <dbghelp.h>
#include "miniDumper.h"

#ifdef UNICODE
    #define _tcssprintf wsprintf
    #define tcsplitpath _wsplitpath
#else
    #define _tcssprintf sprintf
    #define tcsplitpath _splitpath
#endif

const int USER_DATA_BUFFER_SIZE = 4096;

//-----------------------------------------------------------------------------
// GLOBALS
//-----------------------------------------------------------------------------

CMiniDumper* CMiniDumper::s_pMiniDumper = NULL;
LPCRITICAL_SECTION CMiniDumper::s_pCriticalSection = NULL;

// Based on dbghelp.h
typedef BOOL (WINAPI *MINIDUMPWRITEDUMP)(HANDLE hProcess,
                                         DWORD dwPid,
                                         HANDLE hFile,
                                         MINIDUMP_TYPE DumpType,
                                         CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
                                         CONST PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
                                         CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam);

//-----------------------------------------------------------------------------
// Name: CMiniDumper()
// Desc: Constructor
//-----------------------------------------------------------------------------
CMiniDumper::CMiniDumper( bool bPromptUserForMiniDump )
{
	// Our CMiniDumper should act alone as a singleton.
	assert( !s_pMiniDumper );

	memset(m_szMiniDumpPath, 0, sizeof(m_szMiniDumpPath));
	memset(m_szAppPath, 0, sizeof(m_szAppPath));
	memset(m_szAppBaseName, 0, sizeof(m_szAppBaseName));
	memset(m_szPipeName, 0, sizeof(m_szPipeName));



    s_pMiniDumper = this;
    m_bPromptUserForMiniDump = bPromptUserForMiniDump;
	m_IsRemoteDump = false;

    // The SetUnhandledExceptionFilter function enables an application to 
    // supersede the top-level exception handler of each thread and process.
    // After calling this function, if an exception occurs in a process 
    // that is not being debugged, and the exception makes it to the 
    // unhandled exception filter, that filter will call the exception 
    // filter function specified by the lpTopLevelExceptionFilter parameter.
	::SetUnhandledExceptionFilter( unhandledExceptionHandler );

    // Since DBGHELP.dll is not inherently thread-safe, making calls into it 
    // from more than one thread simultaneously may yield undefined behavior. 
    // This means that if your application has multiple threads, or is 
    // called by multiple threads in a non-synchronized manner, you need to  
    // make sure that all calls into DBGHELP.dll are isolated via a global
    // critical section.
    s_pCriticalSection = new CRITICAL_SECTION;

    if( s_pCriticalSection )
        InitializeCriticalSection( s_pCriticalSection );
}

BOOL CMiniDumper::OpenRemoteDump(const TCHAR* strPipeName)
{
	_tcsncpy(m_szPipeName, strPipeName, MAX_PATH);
	m_IsRemoteDump = true;
	return TRUE;
}


//-----------------------------------------------------------------------------
// Name: ~CMiniDumper()
// Desc: Destructor
//-----------------------------------------------------------------------------
CMiniDumper::~CMiniDumper( void )
{
    if( s_pCriticalSection )
    {
        DeleteCriticalSection( s_pCriticalSection );
        delete s_pCriticalSection;
    }
}

//-----------------------------------------------------------------------------
// Name: unhandledExceptionHandler()
// Desc: Call-back filter function for unhandled exceptions
//-----------------------------------------------------------------------------
LONG CMiniDumper::unhandledExceptionHandler( _EXCEPTION_POINTERS *pExceptionInfo )
{
	if( !s_pMiniDumper )
		return EXCEPTION_CONTINUE_SEARCH;
	AfxMessageBox(_T("XX"));
	//开启了远程dump
	_MINIDUMP_EXCEPTION_INFORMATION ExInfo;
	ExInfo.ThreadId = ::GetCurrentThreadId();
	ExInfo.ExceptionPointers = pExceptionInfo;
	ExInfo.ClientPointers = NULL;

	if (true == s_pMiniDumper->m_IsRemoteDump)
	{
		return s_pMiniDumper->writeRemoteDump(&ExInfo, GetCurrentProcess(), GetCurrentProcessId());
	}

	return s_pMiniDumper->writeMiniDump( &ExInfo, GetCurrentProcess(), GetCurrentProcessId());
}

//-----------------------------------------------------------------------------
// Name: setMiniDumpFileName()
// Desc: 
//-----------------------------------------------------------------------------
void CMiniDumper::setMiniDumpFileName( void )
{
    time_t currentTime;
    time( &currentTime );

	TCHAR *pPipeName = m_szPipeName;

	if (true == m_IsRemoteDump && nullptr != pPipeName)
	{
		_tcssprintf(m_szMiniDumpPath,
			_T("%s%s.%ld.dmp"),
			m_szAppPath,
			m_szPipeName,
			currentTime);
	}
	else
	{
		_tcssprintf(m_szMiniDumpPath,
			_T("%s%s.%ld.dmp"),
			m_szAppPath,
			m_szAppBaseName,
			currentTime);
	}
    
}

//-----------------------------------------------------------------------------
// Name: getImpersonationToken()
// Desc: The method acts as a potential workaround for the fact that the 
//       current thread may not have a token assigned to it, and if not, the 
//       process token is received.
//-----------------------------------------------------------------------------
bool CMiniDumper::getImpersonationToken( HANDLE* phToken )
{
    *phToken = NULL;

    if( !OpenThreadToken(GetCurrentThread(),
                          TOKEN_QUERY | TOKEN_ADJUST_PRIVILEGES,
                          TRUE,
                          phToken) )
    {
		DWORD dw = GetLastError();
        if( dw == ERROR_NO_TOKEN )
        {
            // No impersonation token for the current thread is available. 
            // Let's go for the process token instead.
            if( !OpenProcessToken(GetCurrentProcess(),
                                   TOKEN_QUERY | TOKEN_ADJUST_PRIVILEGES,
                                   phToken) )
                return false;
        }
        else
            return false;
    }

    return true;
}

bool CMiniDumper::getImpersonationToken(HANDLE* phToken, HANDLE hProcess)
{
	*phToken = NULL;

	// No impersonation token for the current thread is available. 
	// Let's go for the process token instead.
	if (!OpenProcessToken(hProcess,
		TOKEN_QUERY | TOKEN_ADJUST_PRIVILEGES,
		phToken))
		return false;


	return true;
}

//-----------------------------------------------------------------------------
// Name: enablePrivilege()
// Desc: Since a MiniDump contains a lot of meta-data about the OS and 
//       application state at the time of the dump, it is a rather privileged 
//       operation. This means we need to set the SeDebugPrivilege to be able 
//       to call MiniDumpWriteDump.
//-----------------------------------------------------------------------------
BOOL CMiniDumper::enablePrivilege( LPCTSTR pszPriv, HANDLE hToken, TOKEN_PRIVILEGES* ptpOld )
{
    BOOL bOk = FALSE;

    TOKEN_PRIVILEGES tp;
    tp.PrivilegeCount = 1;
    tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    bOk = LookupPrivilegeValue( 0, pszPriv, &tp.Privileges[0].Luid );

    if( bOk )
    {
        DWORD cbOld = sizeof(*ptpOld);
        bOk = AdjustTokenPrivileges( hToken, FALSE, &tp, cbOld, ptpOld, &cbOld );
    }

    return (bOk && (ERROR_NOT_ALL_ASSIGNED != GetLastError()));
}

//-----------------------------------------------------------------------------
// Name: restorePrivilege()
// Desc: 
//-----------------------------------------------------------------------------
BOOL CMiniDumper::restorePrivilege( HANDLE hToken, TOKEN_PRIVILEGES* ptpOld )
{
    BOOL bOk = AdjustTokenPrivileges(hToken, FALSE, ptpOld, 0, NULL, NULL);
    return ( bOk && (ERROR_NOT_ALL_ASSIGNED != GetLastError()) );
}

//-----------------------------------------------------------------------------
// Name: writeMiniDump()
// Desc: 
//-----------------------------------------------------------------------------
LONG CMiniDumper::writeMiniDump(_MINIDUMP_EXCEPTION_INFORMATION *pExceptionInfo, HANDLE hHandle, DWORD dwProcessID, BOOL bIsRemoteDump)
{
	LONG retval = EXCEPTION_CONTINUE_SEARCH;
	m_pExceptionInfo = pExceptionInfo->ExceptionPointers;

    HANDLE hImpersonationToken = NULL;

	if (TRUE == bIsRemoteDump)
	{
		if (!getImpersonationToken(&hImpersonationToken, hHandle))
			return FALSE;
	}
	else
	{
		if (!getImpersonationToken(&hImpersonationToken))
			return FALSE;
	}
	
	// You have to find the right dbghelp.dll. 
	// Look next to the EXE first since the one in System32 might be old (Win2k)
	
	HMODULE hDll = NULL;
	TCHAR szDbgHelpPath[MAX_PATH];

	if( GetModuleFileName( NULL, m_szAppPath, _MAX_PATH ) )
	{
		TCHAR *pSlash = _tcsrchr( m_szAppPath, '\\' );

		if( pSlash )
		{
			_tcscpy_s( m_szAppBaseName, pSlash + 1);
			*(pSlash+1) = 0;
		}

		_tcscpy_s( szDbgHelpPath, m_szAppPath );
        _tcscat_s( szDbgHelpPath, _T("DBGHELP.DLL") );
		hDll = ::LoadLibrary( szDbgHelpPath );
	}

	if( hDll == NULL )
	{
		// If we haven't found it yet - try one more time.
		hDll = ::LoadLibrary( _T("DBGHELP.DLL") );
	}

	LPCTSTR szResult = NULL;

	if( hDll )
	{
        // Get the address of the MiniDumpWriteDump function, which writes 
        // user-mode mini-dump information to a specified file.
		MINIDUMPWRITEDUMP MiniDumpWriteDump = 
            (MINIDUMPWRITEDUMP)::GetProcAddress( hDll, "MiniDumpWriteDump" );

		if( MiniDumpWriteDump != NULL )
        {
			TCHAR szScratch[USER_DATA_BUFFER_SIZE];

			setMiniDumpFileName();

			// Ask the user if he or she wants to save a mini-dump file...
			_tcssprintf( szScratch,
                         _T("There was an unexpected error:\n\nWould you ")
                         _T("like to create a mini-dump file?\n\n%s " ),
                         m_szMiniDumpPath);

			// Create the mini-dump file...
			HANDLE hFile = ::CreateFile( m_szMiniDumpPath, 
                                            GENERIC_WRITE, 
                                            FILE_SHARE_WRITE, 
                                            NULL, 
                                            CREATE_ALWAYS, 
                                            FILE_ATTRIBUTE_NORMAL, 
                                            NULL );

			if( hFile != INVALID_HANDLE_VALUE )
			{

                // We need the SeDebugPrivilege to be able to run MiniDumpWriteDump
                TOKEN_PRIVILEGES tp;
                BOOL bPrivilegeEnabled = enablePrivilege( SE_DEBUG_NAME, hImpersonationToken, &tp );

                BOOL bOk;

				// DBGHELP.dll is not thread-safe, so we need to restrict access...
				EnterCriticalSection(s_pCriticalSection);
				{
					// Write out the mini-dump data to the file...
					if (bIsRemoteDump)
					{
						_MINIDUMP_EXCEPTION_INFORMATION ex_info;
						SIZE_T nReadCount = 0;
						if (ReadProcessMemory(hHandle,
							&ex_info,
							pExceptionInfo,
							sizeof(_MINIDUMP_EXCEPTION_INFORMATION),
							&nReadCount)
							)
						{
							if (nReadCount == sizeof(_MINIDUMP_EXCEPTION_INFORMATION))
							{
								ex_info.ClientPointers = FALSE;
								bOk = MiniDumpWriteDump(hHandle,
									dwProcessID,
									hFile,
									MiniDumpNormal,
									pExceptionInfo,
									NULL,
									NULL);
							}
						}

					}
					else
					{
						bOk = MiniDumpWriteDump(hHandle,
							dwProcessID,
							hFile,
							MiniDumpNormal,
							pExceptionInfo,
							NULL,
							NULL);
					}	
				}
				LeaveCriticalSection(s_pCriticalSection);

                if( bOk )
				{
					szResult = NULL;
					retval = EXCEPTION_EXECUTE_HANDLER;
				}
				else
				{
					DWORD dw = GetLastError();
					_tcssprintf( szScratch,
                                    _T("Failed to save the mini-dump file to '%s' (error %d)"),
                                    m_szMiniDumpPath,
                                    GetLastError() );

					szResult = szScratch;
				}

				// Restore the privileges when done
				if (bPrivilegeEnabled)
					restorePrivilege(hImpersonationToken, &tp);

				::CloseHandle( hFile );
			}
			else
			{
				_tcssprintf( szScratch,
                                _T("Failed to create the mini-dump file '%s' (error %d)"),
                                m_szMiniDumpPath,
                                GetLastError() );

				szResult = szScratch;
			}
		}
		else
		{
			szResult = _T( "Call to GetProcAddress failed to find MiniDumpWriteDump. ")
                       _T("The DBGHELP.DLL is possibly outdated." );
		}
	}
	else
	{
		szResult = _T( "Call to LoadLibrary failed to find DBGHELP.DLL." );
	}

	if( szResult && m_bPromptUserForMiniDump )
		::MessageBox( NULL, szResult, NULL, MB_OK );

	TerminateProcess(hHandle, 0 );

	return retval;
}


//-----------------------------------------------------------------------------
// Name: writeMiniDump()
// Desc: 
//-----------------------------------------------------------------------------
LONG CMiniDumper::writeRemoteDump(_MINIDUMP_EXCEPTION_INFORMATION *pExceptionInfo, HANDLE hHandle, DWORD dwProcessID)
{
	BOOL bRemoteDump = FALSE;
	LONG retval = EXCEPTION_CONTINUE_SEARCH;
	m_pExceptionInfo = pExceptionInfo->ExceptionPointers;
	HANDLE hPipe = NULL;

	// DBGHELP.dll is not thread-safe, so we need to restrict access...
	EnterCriticalSection(s_pCriticalSection);
	{
		// Write out the mini-dump data to the file...
		char buf[256] = { 'm', 'e', 'm', 'o', 'r', 'y', '_', 'd', 'u', 'm', 'p', '\r', '\n' };
		
		DWORD dwThreadId = ::GetCurrentThreadId();
		memcpy(buf + 13, &dwProcessID, sizeof(DWORD));
		memcpy(buf + 13 + sizeof(DWORD), &dwThreadId, sizeof(DWORD));
		memcpy(buf + 13 + sizeof(DWORD) + sizeof(DWORD), pExceptionInfo, sizeof(_MINIDUMP_EXCEPTION_INFORMATION));
		DWORD nWrite = 0;
		int nPos = 0;
		int nTotal = 13 + sizeof(_MINIDUMP_EXCEPTION_INFORMATION) + sizeof(DWORD) + sizeof(DWORD);

		while (1)
		{
			CString strPipName = m_szPipeName;
			CString strObjtectName = _T("\\\\.\\Pipe\\");
			strObjtectName += strPipName;
			//BOOL bRet = WaitNamedPipe(strObjtectName, NMPWAIT_WAIT_FOREVER);
			//if (false == bRet)
			//{
			//	//AfxMessageBox(L"连接管道失败");
			//	return;
			//}
			hPipe = CreateFile(			//管道属于一种特殊的文件
				strObjtectName,	//创建的文件名
				GENERIC_READ | GENERIC_WRITE,	//文件模式
				0,								//是否共享
				NULL,							//指向一个SECURITY_ATTRIBUTES结构的指针
				OPEN_EXISTING,					//创建参数
				FILE_ATTRIBUTE_NORMAL,			//文件属性(隐藏,只读)NORMAL为默认属性
				NULL);

			if (INVALID_HANDLE_VALUE == hPipe)
			{
				DWORD dw = GetLastError();
				continue;

			}
			BOOL bRet = WriteFile(hPipe, buf + nPos, nTotal, &nWrite, NULL);
			if (!bRet)
			{
				AfxMessageBox(_T("写管道失败"));
				break;
			}
			else
			{
				nPos += nWrite;
				if (nTotal == nPos)
				{
					break;
				}
				Sleep(500);
			}
		}
		AfxMessageBox(_T("写通道完成"));
		DWORD dwBegin = GetTickCount();
		while (1)
		{
			char bufRead[256];
			DWORD nRead = 0;
			if (ReadFile(hPipe, buf, 256, &nRead, NULL))
			{
				bRemoteDump = TRUE;
				break;
			}
			DWORD dwEnd = GetTickCount();

			if ((dwEnd - dwBegin) > 1000 * 10)
			{
				bRemoteDump = FALSE;	
				break;
			}
			Sleep(500);
		}

	}
	CloseHandle(hPipe);
	LeaveCriticalSection(s_pCriticalSection);

	//不能远程dump,则开始本地dump
	if (!bRemoteDump)
	{
		writeMiniDump(pExceptionInfo, hHandle, dwProcessID);
	}

	if (m_bPromptUserForMiniDump)
	{
		//::MessageBox(NULL, szResult, NULL, MB_OK);
	}
		
	TerminateProcess(hHandle, 0);

	return retval;
}
