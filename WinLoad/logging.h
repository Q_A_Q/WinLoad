#ifndef _WIN_LOAD_LOGGING_H__
#define _WIN_LOAD_LOGGING_H__

#include <boost\log\core.hpp>
#include <boost\log\trivial.hpp>
#include <boost\shared_ptr.hpp>
#include <string>
#include <boost/log/attributes/named_scope.hpp>


namespace  duan
{
	enum severity_level {
		SL_TRACE = 0,
		SL_DEBUG,
		SL_INFO,
		SL_WARNING,
		SL_ERROR,
		SL_CRITICAL
	};

class logging
{
public:
	logging(std::string strFileName);
	~logging();

	static bool InitBoostLog(std::string strFileName);

	static bool CloseBoostLog();

	static boost::log::sources::severity_logger<severity_level> m_slg;
};

#define BOOST_SLOG(slog_lvl) BOOST_LOG_FUNCTION();BOOST_LOG_SEV(duan::logging::m_slg, slog_lvl)

#define SLOG_TRACE BOOST_SLOG(duan::severity_level::SL_TRACE)
#define SLOG_DEBUG BOOST_SLOG(duan::severity_level::SL_DEBUG)
#define SLOG_INFO BOOST_SLOG(duan::severity_level::SL_INFO)
#define SLOG_WARNING BOOST_SLOG(duan::severity_level::SL_WARNING)
#define SLOG_ERROR BOOST_SLOG(duan::severity_level::SL_ERROR)
#define SLOG_CRITICAL BOOST_SLOG(duan::severity_level::SL_CRITICAL)

} //end namespace

#endif // !_WIN_LOAD_LOGGING_H__
