
// WinLoadDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "WinLoad.h"
#include "WinLoadDlg.h"
#include "afxdialogex.h"
#include "WinLoadIni.h"
#include <windows.h>
#include <TlHelp32.h>
#include <dbghelp.h>
#include "util.h"
#include <boost\thread.hpp>
#include "logging.h"
#include "MiniDumper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define LOAD_INI_PATH ("\\load.ini")

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CWinLoadDlg 对话框



CWinLoadDlg::CWinLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_WINLOAD_DIALOG, pParent),
	m_eventExit(TRUE, TRUE)
{
	m_threadPipRead = nullptr;
	m_hHandle = nullptr;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWinLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CWinLoadDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_DEBUG_PIP, &CWinLoadDlg::OnBnClickedButtonDebugPip)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CWinLoadDlg 消息处理程序

BOOL CWinLoadDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码


	//加载配置文件
	LoadProp();

	//进行检测程序系统设置
	SystemSetting();
	
	//开始检测
	BeginWatch();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CWinLoadDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CWinLoadDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CWinLoadDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

bool CWinLoadDlg::CreatePipe()
{
	m_mutexCreatePipe.lock();

	std::string strPipName = (*CSingleton::GetInstance())["object"]["object_pip"].as<std::string>();
	if (strPipName.empty())
	{
		m_mutexCreatePipe.unlock();
		return false;
	}

	CString strObjtectName = TEXT("\\\\.\\Pipe\\");
	strObjtectName += strPipName.c_str();

	if (nullptr != m_hHandle && INVALID_HANDLE_VALUE != m_hHandle)
	{
		CloseHandle(m_hHandle);
	}

	m_hHandle = CreateNamedPipe(strObjtectName,
		PIPE_ACCESS_DUPLEX,										//管道类型，双向通信  
		PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,  //管道参数  
		PIPE_UNLIMITED_INSTANCES,								//管道能创建的最大实例数量  
		0,														//输出缓冲区长度 0表示默认  
		0,														//输入缓冲区长度 0表示默认  
		0,													//超时时间,NMPWAIT_WAIT_FOREVER为不限时等待
		NULL);
	if (INVALID_HANDLE_VALUE == m_hHandle)
	{
		SLOG_ERROR << "创建命名管道失败:" << GetLastError();
		m_mutexCreatePipe.unlock();
		return false;
	}
	m_mutexCreatePipe.unlock();
	return true;
}


void CWinLoadDlg::ThreadPipRead(void *pThis)
{
	SLOG_INFO << "管道读取线程开始";
	CWinLoadDlg *myThis = (CWinLoadDlg *)pThis;

	int nReadPipeInterval = (*CSingleton::GetInstance())["object"]["object_pip_time"].as<int>();
	bool bRet = this->CreatePipe();
	if (bRet)
	{

		int nPipTimeOut = (*CSingleton::GetInstance())["object"]["object_pip_time"].as<int>();
		m_nPipReadMaxInterval = (*CSingleton::GetInstance())["object"]["object_pip_read_max"].as<int>();

		//if (!ConnectNamedPipe(m_hHandle, &overLapped))						//阻塞等待客户端连接。  
		//{
		//	//AfxMessageBox(L"连接管道失败");
		//	CloseHandle(m_hHandle);
		//	m_hHandle = nullptr;
		//	return;
		//}
		m_eventExit.ResetEvent();
		HANDLE hArray[] = { m_eventExit };

		while (WAIT_OBJECT_0 + 0 != WaitForMultipleObjects(sizeof(hArray)/sizeof(HANDLE), hArray, FALSE, nReadPipeInterval))
		{
			char buf[256] = { 0 };
			DWORD nRead = 0;

			BOOL bRead = ReadFile(m_hHandle, buf, 256, &nRead, NULL);
			if (false == bRead)
			{	
				DWORD dw = GetLastError();
				SLOG_DEBUG << "管程读取失败" << dw;
				m_nPipReadMaxInterval--;

				if (dw == 0 || dw == 536/*等待连接中*/)
				{
				}
				else
				{
					//重新建立管道
					this->CreatePipe();
				}
				
			}
			else
			{
				std::string strMsg(buf);
				std::string strHeartBeat = "heart_beat";
				std::string strMemoryDump = "memory_dump";


				int nPos = strMsg.find("\r\n");
				if (nPos <= 0 || nPos > 255)
				{
					break;
				}

				//心跳报文
				if (strMsg.compare(0, strHeartBeat.size(), strHeartBeat, 0, nPos) == 0)
				{
					//不处理
					
				}
				else if (strMsg.compare(0, strMemoryDump.size(), strMemoryDump, 0, nPos) == 0)//minidump
				{
					char *pBaseAddress = (char *)(buf + nPos + 2);
					DWORD *dwProcessID = (DWORD *)(pBaseAddress);
					DWORD *dwThreadID = (DWORD *)(pBaseAddress + sizeof(DWORD));
					_MINIDUMP_EXCEPTION_INFORMATION *pExceptionInfo = (_MINIDUMP_EXCEPTION_INFORMATION *)(pBaseAddress + sizeof(DWORD) + sizeof(DWORD));
					if (nullptr == CMiniDumper::s_pMiniDumper)
					{

						SLOG_ERROR << "s_pMiniDumper为空";
						continue;
					}
	
					//打开进程
					HANDLE hProcess = ::OpenProcess(PROCESS_TERMINATE | PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, *dwProcessID);

					//打开远程dump
					std::string strCheckName = (*CSingleton::GetInstance())["object"]["object_name"].as<std::string>();


					#ifdef  UNICODE
					 CMiniDumper::s_pMiniDumper->OpenRemoteDump(c2w(strCheckName.c_str()).c_str());
					#else
					CMiniDumper::s_pMiniDumper->OpenRemoteDump(strCheckName.c_str());
					#endif

					//写dump文件
					CMiniDumper::s_pMiniDumper->writeMiniDump(pExceptionInfo, hProcess, *dwThreadID, TRUE);

				}
				else //处理未识别的消息
				{
					SLOG_ERROR << "未知消息:" << strMsg;
				}

				m_nPipReadMaxInterval = (*CSingleton::GetInstance())["object"]["object_pip_read_max"].as<int>();
			}

			//失败N次之后，可以确定该进程其实已经卡死了
			if ( m_nPipReadMaxInterval <= 0)
			{
				DWORD dwProcessID = 0;
				//杀掉原来的进程
				bool bSuc = ScanTaskManager((*CSingleton::GetInstance())["object"]["object_name"].as<std::string>(), dwProcessID, true);

				//杀掉之后重新扫描。保证系统已经杀掉了该进程
				bSuc = ScanTaskManager((*CSingleton::GetInstance())["object"]["object_name"].as<std::string>(), dwProcessID);
				if (true == bSuc)
				{
					SLOG_ERROR << "没有杀掉检测进程";
					continue;
				}

				//启动该进程
				CStringA strPath((*CSingleton::GetInstance())["object"]["object_path"].as<std::string>().c_str());
				strPath = strPath.Trim();
				if (strPath.GetAt(0) == '.')//为相对路径
				{
					HMODULE hModule = GetModuleHandle(NULL);
					char modulePath[MAX_PATH] = { 0 };
					GetModuleFileNameA(hModule, modulePath, MAX_PATH);
					CStringA strBasePath(modulePath);
					int nPos = strBasePath.ReverseFind('\\');
					if (nPos <= 0)
					{
						break;
					}
					CStringA strExePath = strBasePath.Mid(0, nPos) + "\\" + strPath;
					ShellExecuteA(nullptr, "open", strExePath, NULL, NULL, SW_SHOWNORMAL);
				}
				else
				{
					ShellExecuteA(nullptr, "open", strPath, NULL, NULL, SW_SHOWNORMAL);
				}
				

				//等待500毫秒，保证目标启动
				Sleep(500);

				//再扫描一次， 保证系统已经启动了
				bSuc = ScanTaskManager((*CSingleton::GetInstance())["object"]["object_name"].as<std::string>(), dwProcessID);

				//不论成功与否，都应该重置。不然会一直去读写了
				m_nPipReadMaxInterval = (*CSingleton::GetInstance())["object"]["object_pip_read_max"].as<int>();

			}
		}

		SLOG_INFO << "退出管道读取线程";
		CloseHandle(m_hHandle);
		m_hHandle = nullptr;
	}
}


bool CWinLoadDlg::LoadProp()
{
	HMODULE hModule = GetModuleHandle(NULL);
	char modulePath[MAX_PATH] = { 0 };
	GetModuleFileNameA(hModule, modulePath, MAX_PATH);
	CStringA strPath(modulePath);
	int nPos = strPath.ReverseFind('\\');
	if (nPos <= 0)
	{
		return false;
	}
	CStringA strIniPath = strPath.Mid(0, nPos) + LOAD_INI_PATH;

	CSingleton::GetInstance()->load(strIniPath.GetBuffer());
	strIniPath.ReleaseBuffer();
	return true;
}

void CWinLoadDlg::BeginWatch()
{
	//设置扫描任务管理器的时间间隔
	int nScanTaskManagerTime = (*CSingleton::GetInstance())["object"]["scan_task_manager_time"].as<int>();
	nScanTaskManagerTime *= 1000;
	//SetTimer(TE_ScanTaskManager, nScanTaskManagerTime, nullptr);

	//开始管道进程监控
	m_threadPipRead = new boost::thread(boost::bind(&CWinLoadDlg::ThreadPipRead, this, this));
	
}


bool CWinLoadDlg::ScanTaskManager(std::string strTaskName, DWORD &dwProcessID, bool bKillThread)
{
	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(pe32);

	HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE) {
		//std::cout << "CreateToolhelp32Snapshot Error!" << std::endl;;
		return false;
	}

	BOOL bResult = Process32First(hProcessSnap, &pe32);

	int num(0);

	while (bResult)
	{
#ifdef  UNICODE
		std::string name = w2c(pe32.szExeFile);
#else
		std::string name = pe32.szExeFile;
#endif
		int id = pe32.th32ProcessID;
		
		if (name == strTaskName)
		{
			if (true == bKillThread)
			{
				HANDLE hProcess = ::OpenProcess(PROCESS_TERMINATE, FALSE, id);
				::TerminateProcess(hProcess, 0);
			}
			dwProcessID = id;
			CloseHandle(hProcessSnap);
			return true;
		}

		//_nameID.insert(std::pair<string, int>(name, id)); //字典存储
		bResult = Process32Next(hProcessSnap, &pe32);
	}

	CloseHandle(hProcessSnap);

	return false;
}


bool CWinLoadDlg::SystemSetting()
{

	return true;
}

void CWinLoadDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	switch (nIDEvent)
	{
	case TE_ScanTaskManager:
	{
		std::string strCheckName = (*CSingleton::GetInstance())["object"]["object_name"].as<std::string>();
		DWORD dwProcessID = 0;
		bool bHaveThread = ScanTaskManager(strCheckName, dwProcessID);
		if (false == bHaveThread)
		{
			SLOG_INFO << "检测到监控软件没有启动:" << strCheckName;
			CStringA strPath((*CSingleton::GetInstance())["object"]["object_path"].as<std::string>().c_str());
			strPath = strPath.Trim();
			if (strPath.GetAt(0) == '.')//为相对路径
			{
				HMODULE hModule = GetModuleHandle(NULL);
				char modulePath[MAX_PATH] = { 0 };
				GetModuleFileNameA(hModule, modulePath, MAX_PATH);
				CStringA strBasePath(modulePath);
				int nPos = strBasePath.ReverseFind('\\');
				if (nPos <= 0)
				{
					break;
				}
				CStringA strExePath = strBasePath.Mid(0, nPos) + "\\" + strPath;
				ShellExecuteA(nullptr, "open", strExePath, NULL, NULL, SW_SHOWNORMAL);
			}
			else
			{
				ShellExecuteA(nullptr, "open", strPath, NULL, NULL, SW_SHOWNORMAL);
			}
		}
	}
	break;

	//通过管道获取目标状态，主要是为了当进程出现卡死
	case TE_PipReadInterval: //管道改为使用单独线程去处理，不使用定时器处理。因为会卡死主线程
	{
		
	}

	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CWinLoadDlg::OnBnClickedButtonDebugPip()
{
	// TODO: 在此添加控件通知处理程序代码
	//
	std::string strPipName = (*CSingleton::GetInstance())["object"]["object_pip"].as<std::string>();
	CString strObjtectName = TEXT("\\\\.\\Pipe\\");
	strObjtectName += strPipName.c_str();
	BOOL bRet = WaitNamedPipe(strObjtectName, NMPWAIT_WAIT_FOREVER);
	if (false == bRet)
	{
		SLOG_DEBUG << "连接管道失败" <<GetLastError();
		return;
	}

	HANDLE hPipe = CreateFile(			//管道属于一种特殊的文件
		strObjtectName,	//创建的文件名
		GENERIC_READ | GENERIC_WRITE,	//文件模式
		0,								//是否共享
		NULL,							//指向一个SECURITY_ATTRIBUTES结构的指针
		OPEN_EXISTING,					//创建参数
		FILE_ATTRIBUTE_NORMAL,			//文件属性(隐藏,只读)NORMAL为默认属性
		NULL);

	if (INVALID_HANDLE_VALUE == hPipe)
	{
		SLOG_DEBUG << "打开管道失败:" << GetLastError();
	}

	char buf[] = { 'h', 'e', 'a', 'r', 't' };
	DWORD nWrite = 0;
	if (!WriteFile(hPipe, buf, sizeof(buf), &nWrite, NULL))			//接受客户端发送数据
	{
		DWORD dw = GetLastError();
		SLOG_DEBUG << "写管道失败:" <<GetLastError();
		return;
	}
}


void CWinLoadDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	m_eventExit.SetEvent();
	if (nullptr != m_threadPipRead)
	{
		m_threadPipRead->join();
		delete m_threadPipRead;
		m_threadPipRead = nullptr;
	}


	SLOG_INFO << "关闭软件监控";
	CDialogEx::OnClose();
}
